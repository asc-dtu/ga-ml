from random import random, shuffle, sample
from ase import Atoms
from ase.io import read
from ase.ga.data import PrepareDB

atoms = read('random_relaxed.traj')
atoms.cell[0][0] = 32.0
atoms.cell[1][1] = 32.0
atoms.cell[2][2] = 32.0
atoms.center()
population_size = 150
gen_size = population_size

cluster_size = len(atoms)

sp = []
for _ in range(gen_size):
    indi = atoms.copy()
    no_au = int(random()*145)+1
    to_au = sample(range(cluster_size), no_au)
    indi.symbols[to_au] = 'Au'
    sp.append(indi)
    # for i in range(no_au):
    #     indi[i].symbol = 'Au'
    # notPt = sorted([a.index for a in atoms if a.symbol != 'Mo'], reverse=True)
    # anew = Atoms()
    # for i in notPt:
    #     anew.append(indi.pop(i))
    # anew.extend(indi)
    # nums = anew[:147].numbers
    # shuffle(nums)
    # for i in range(len(nums)):
    #     anew[i].number = nums[i]
    # sp.append(anew)


db = PrepareDB('ridge.db', cell=atoms.get_cell(),
               population_size=population_size)

for a in sp:
    db.add_unrelaxed_candidate(a)
