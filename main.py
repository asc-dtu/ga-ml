from asap3 import EMT as asapEMT
from ase.ga.data import DataConnection
from ase.ga.particle_mutations import RandomPermutation, COM2surfPermutation, Rich2poorPermutation, Poor2richPermutation, SymmetricSubstitute, RandomSubstitute
from ase.ga.particle_crossovers import CutSpliceCrossover
from ase.ga.particle_comparator import NNMatComparator
from ase.ga.standard_comparators import RawScoreComparator, EnergyComparator, NoComparator
from ase.ga.offspring_creator import OperationSelector
from ase.ga.population import RankFitnessPopulation, Population, FitnessSharingPopulation
from ase.ga.convergence import GenerationRepetitionConvergence
from ase.ga.utilities import closest_distances_generator, get_nnmat
from ase.ga.particle_mutations import Mutation
from ase import Atoms
from ase.data import atomic_masses, atomic_numbers

from calc import relax
from random import choices, sample, randint
import string
import shutil
import os
import numpy as np
from operator import itemgetter
from collections import defaultdict

import re
import numpy.linalg as la
import scipy.stats as ss
from math import *
from numpy import *
import random

from ase.ga import get_raw_score
from coulomb import eig_data
from small_ga import SmallGA


def vf(x):
    """Returns the descriptor that distinguishes candidates in the niched
    population."""
    return x.get_chemical_formula(mode='hill')


restart = True
small_ga_r = 1.0

db_name = 'ridge.db'

# Instantiate the db
# Copy the db to /scratch in order for faster io
if 'SLURM_JOB_ID' in os.environ:
    # The script is submitted
    dname = ''.join(choices(string.ascii_uppercase + string.digits, k=6))
    fname = "/scratch/$USER/"+dname+"-"+db_name
    shutil.copyfile(db_name, fname)
    db = DataConnection(fname)
else:
    db = DataConnection(db_name)


# Define operators
cd = closest_distances_generator(atom_numbers=[79, 78],
                                 ratio_of_covalent_radii=0.7)
oclist = ([2, 2, 6], [RandomPermutation(['Au', 'Pt']),
                      RandomSubstitute(elements=['Au', 'Pt']),
                      CutSpliceCrossover(cd, keep_composition=False)])
soclist = ([2, 1, 1, 6], [RandomPermutation(['Au', 'Pt'],
                                            num_muts=randint(1, 10)),
                          RandomSubstitute(elements=['Au', 'Pt']),
                          SymmetricSubstitute(elements=['Au', 'Pt']),
                          CutSpliceCrossover(cd, keep_composition=False)])
op_selector = OperationSelector(*oclist)

comp = NNMatComparator(0.2, ['Au', 'Pt'])

# Define population
pop_size = db.get_param('population_size')
pop = RankFitnessPopulation(data_connection=db,
                            population_size=pop_size,
                            comparator=comp,
                            variable_function=vf,
                            exp_function=True,
                            logfile='log.txt')

# Convergence criteria
cc = GenerationRepetitionConvergence(pop, 5)

# Sort out awful candidates
traj_list = []
all_trajs = db.get_all_relaxed_candidates()
for c in all_trajs:
    if get_raw_score(c) > -6.:
        traj_list.append(c)

# Build nnmat list for comparing new candidates with existing
nnmat_comp_list = defaultdict(list)
if restart:
    for cnn in all_trajs:
        nncomp = cnn.get_chemical_formula(mode='hill')
        resnn = get_nnmat(cnn)
        nnmat_comp_list[nncomp].append(resnn)

# Relax starting generation.
while db.get_number_of_unrelaxed_candidates() > 0:
    a = db.get_an_unrelaxed_candidate()
    nncomp = a.get_chemical_formula(mode='hill')
    a_nnmat = get_nnmat(a)
    nnmat_comp_list[nncomp].append(a_nnmat)
    relax(a)
    db.add_relaxed_step(a)
pop.update()


all_Pt_mass = atomic_masses[atomic_numbers['Pt']] * 147


def finger(r):
    resnn = get_nnmat(r)
    # Normalized mass
    nm = (sum(r.get_masses()) - all_Pt_mass) / 270.0
    # Normalized number of neighbors
    nn = 0
    for nmn in resnn:
        nn = nn+1
        var = float(nmn)
        if nn == 1:
            mat1 = var/12.
        if nn == 2:
            mat2 = var/12.
        if nn == 3:
            mat3 = var/12.
        if nn == 4:
            mat4 = var/12.
    m5finger = np.array(mat1+mat2)
    m6finger = np.array(mat3+mat4)
    m7finger = np.array(mat1+mat2-mat3-mat4)
    return np.array((mat1, mat2, mat3, mat4, m5finger, m6finger, m7finger, nm))


sigma = 0.4


def kernel(f1, f2):
    return exp(-sum((f1-f2)**2)/(2*sigma**2))


def pf(x):
    """Predict a raw score of the supplied atoms object x. Set the raw
    score and other info as well."""
    gcfinger = np.asarray([finger(x)])
    kT = [[kernel(t1, t2) for t1 in trainfingers] for t2 in gcfinger]
    kTCinv = np.dot(kT, Cinv)
    trainval = sampled_energies
    trainmean = mean(trainval)
    trainval -= trainmean
    testpred = np.dot(kTCinv, trainval)+trainmean

    x.info['key_value_pairs']['raw_score'] = testpred[0]
    x.info['data']['nnmat'] = get_nnmat(x)

    syms = x.get_chemical_symbols()
    x.info['key_value_pairs']['noPt'] = syms.count('Pt')


def is_ok(atoms):
    if len(set(atoms.numbers)) == 1:
        return False
    return True


nt = 400  # number of training images
lamd = 10**(-3.)
g = db.get_generation_number()
ccou = 0
while db.get_generation_number() < 200:
    # Check if concerged.
    if cc.converged():
        print('converged')
        break

    # Get training data set.
    # Get array of known energies for each compositon in database.
    syss = []
    formen = defaultdict(list)
    all_trajs = db.get_all_relaxed_candidates()
    for c in all_trajs:
        ce = get_raw_score(c)
        cf = c.get_chemical_formula(mode='hill')
        if ce > -6.:
            syss.append(c)
            formen[cf].append(ce)

    # Set up training data set to get fingerprint.
    if len(syss) < nt:
        # Actual number of training images used
        ntn = len(syss)
        trainsyss = syss
    else:
        ntn = nt
        trainsyss = [syss[i]
                     for i in sorted(sample(range(len(syss)), nt))]
    sampled_energies = []
    for syse in trainsyss:
        rese = get_raw_score(syse)
        sampled_energies.append(rese)
    trainfingers = np.asarray([finger(sys) for sys in trainsyss])

    # Get relationship between fingers.
    C = np.asarray([[kernel(t1, t2) for t1 in trainfingers]
                    for t2 in trainfingers])
    C = C+lamd*np.identity(ntn)
    Cinv = la.inv(C)

    small_run = False
    if random.random() < small_ga_r and len(db.get_all_relaxed_candidates()) > 2*pop_size:
        sgrname = ''.join(choices(string.ascii_uppercase + string.digits, k=6))
        sgname = "/scratch/$USER/"+sgrname+"-"+db_name
        small_run = True
        gcand = SmallGA(input_candidates=pop.pop,
                        offspring_creator_list=soclist,
                        db_file_name=str(sgname)+'.db',
                        relax_function=pf,
                        population_size=pop_size,
                        population_type=RankFitnessPopulation,
                        population_pool=False,
                        comparator=comp,
                        population_kwargs={"variable_function": vf,
                                           "exp_function": True},
                        offspring_ok_function=is_ok,
                        ).run(steps=20)  # Steps now relates to number of generations,
        # N_cands = steps * pop_size

        gcfinger = np.asarray([finger(sys) for sys in gcand])
        kT = [[kernel(t1, t2) for t1 in trainfingers] for t2 in gcfinger]
        kTCinv = np.dot(kT, Cinv)

        trainval = sampled_energies
        trainmean = mean(trainval)
        trainval -= trainmean
        testpred = np.dot(kTCinv, trainval)+trainmean

    if not small_run:
        # Produce test system of ten candidates.
        gcand = []    # list of potential candidates to test with ML.
        for i in range(400):
            val_cand = False
            while not val_cand:
                found_nnmat = False
                a1, a2 = pop.get_two_candidates()
                op = op_selector.get_operator()
                a3, desc = op.get_new_individual([a1, a2])
                a3_nnmat = get_nnmat(a3)
                nncomp = a3.get_chemical_formula(mode='hill')
                # Test for nnmat similarities.
                if nncomp != 'Pt147' and nncomp != 'Au147':
                    if nncomp in nnmat_comp_list:
                        for cl in nnmat_comp_list[nncomp]:
                            diff = np.linalg.norm(a3_nnmat - cl)
                            if diff < 0.2:
                                found_nnmat = True
                                break
                if not found_nnmat:
                    val_cand = True
            # If unique add to gcand list.
            gcand.append(a3)
        # Get predictions of gcand list based on training data set.
        gcfinger = np.asarray([finger(sys) for sys in gcand])
        kT = [[kernel(t1, t2) for t1 in trainfingers] for t2 in gcfinger]
        kTCinv = np.dot(kT, Cinv)

        trainval = sampled_energies
        trainmean = mean(trainval)
        trainval -= trainmean
        testpred = np.dot(kTCinv, trainval)+trainmean

    # Sort candidates based on prediction.
    sort_pred = []
    for i, j in zip(testpred, gcand):
        sort_pred.append((i, j))
        sort_pred.sort(key=itemgetter(0), reverse=True)
    # Check to see if gcand should be relaxed based on prediction.
    # If gcand is less fit, it is not relaxed.
    for i, j in sort_pred:
        jcf = j.get_chemical_formula(mode='hill')
        # Relax any gcand with unique compositon.
        if jcf not in formen:
            formen[jcf] = []
            if jcf not in nnmat_comp_list:
                nnmat_comp_list[jcf] = []
            jnnmat = get_nnmat(j)
            nnmat_comp_list[jcf].append(jnnmat)
            if small_run:
                relax(j)
                db.add_relaxed_step(j)
            else:
                db.add_unrelaxed_candidate(j, description=desc)
                relax(j)
                db.add_relaxed_step(j)
            ccou += 1
            je = get_raw_score(j)
            formen[jcf].append(je)
            # update population.
            pop.update()
        else:
            # check to see if gcand is better than fittest for given composition.
            me = max(formen[jcf])
            if i >= me:
                found_jnnmat = False
                jnnmat = get_nnmat(j)
                for cl in nnmat_comp_list[jcf]:
                    diff = np.linalg.norm(jnnmat - cl)
                    if diff < 0.2:
                        found_jnnmat = True
                        break
                if jcf not in nnmat_comp_list:
                    nnmat_comp_list[jcf] = []
                if not found_jnnmat:
                    nnmat_comp_list[jcf].append(jnnmat)
                    if small_run:
                        relax(j)
                        db.add_relaxed_step(j)
                    else:
                        db.add_unrelaxed_candidate(j, description=desc)
                        relax(j)
                        db.add_relaxed_step(j)
                    ccou += 1
                    je = get_raw_score(j)
                    formen[jcf].append(je)
                    # Update population.
                    pop.update()
    if ccou > 5:
        ccou = 0
        shutil.copyfile(fname, db_name)
shutil.copyfile(fname, db_name)
