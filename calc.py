from random import random
from asap3 import EMT as asapEMT
from ase.optimize import FIRE
from ase import Atoms
from ase.ga.relax_attaches import VariansBreak
import sys
from operator import itemgetter
from ase.ga.utilities import get_nnmat

from ase.ga.offspring_creator import OperationSelector
from ase.ga.particle_mutations import RandomPermutation
from ase.ga.data import DataConnection
from ase.ga.particle_crossovers import CutSpliceCrossover
from ase.ga.particle_mutations import Mutation
#gac = Mutation.get_atomic_configuration(elements=['Au','Cu'])

pp = {}
#pp['Cu'] = 48.380878
pp['Au'] = 33.633353
pp['Pt'] = 46.110874


def relax(atoms):
    da = DataConnection('ridge.db')
    g = 0  # da.get_generation_number()
    if g == 0:
        atoms.set_calculator(asapEMT())
        opt = FIRE(atoms, logfile=None)
        opt.run(fmax=0.1)

        aconf = Mutation.get_atomic_configuration(atoms, elements=['Au', 'Pt'])
        atomic_conf = ''
        for shell in aconf:
            atomic_conf += Atoms([atoms[int(i)].symbol for i in shell]
                                 ).get_chemical_formula() + '-'
        atomic_conf = atomic_conf[:-1]

        e = atoms.get_potential_energy()
        syms = atoms.get_chemical_symbols()
        for a in set(syms):
            e -= (pp[a] / 147.) * syms.count(a)

        atoms.info['key_value_pairs']['raw_score'] = -e
        atoms.info['key_value_pairs']['EMT_energy'] = -atoms.get_potential_energy()
        atoms.info['data']['nnmat'] = get_nnmat(atoms)
        atoms.set_calculator()

    else:
        htop_mutation = RandomPermutation(['Au','Pt'])

        for j in xrange(100):
            htop_cand = atoms.copy()
            htop_mutation.mutate(htop_cand,elements=['Au','Pt'])
            if htop_cand is not None:
                htop_cand.set_calculator(asapEMT())
                dyn = FIRE(htop_cand, trajectory=None, logfile=None)
                dyn.run(fmax=0.4)
                htop_cand.set_calculator()
            
                update_cand = htop_cand

                atoms.set_calculator(asapEMT())
                atoms_e = atoms.get_potential_energy()
                atoms.set_calculator()

                update_cand.set_calculator(asapEMT())
                update_e = update_cand.get_potential_energy()
                update_cand.set_calculator()

                if update_e < atoms_e:
                    atoms = update_cand


        atoms.set_calculator(asapEMT())
        opt = FIRE(atoms, logfile=None)
        opt.run(fmax=0.1)

        aconf = Mutation.get_atomic_configuration(atoms,elements=['Au','Pt'])
        atomic_conf = ''
        for shell in aconf:
            atomic_conf += Atoms([atoms[int(i)].symbol for i in shell]).get_chemical_formula() + '-'
        atomic_conf = atomic_conf[:-1]

        e = atoms.get_potential_energy()
        syms = atoms.get_chemical_symbols()
        for a in set(syms):
            e -= (pp[a] / 147.) * syms.count(a)
    
        atoms.info['key_value_pairs']['raw_score'] = -e
        atoms.info['key_value_pairs']['EMT_energy'] = -atoms.get_potential_energy()

        atoms.info['data']['nnmat'] = get_nnmat(atoms)
        atoms.set_calculator()
    return atoms
