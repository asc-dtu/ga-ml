from ase import Atoms
from ase.io import read
import numpy as np
import numpy.linalg as linalg

def eig_data(atoms):
    init_R = atoms.positions
    init_Z = atoms.numbers

    x_list = []
    y_list = []
    z_list = []
    M_list = []
    M_tmp = []
    M_matrix = []

    for x,y,z in init_R:
        x_list.append(x)
        y_list.append(y)
        z_list.append(z)

    order=0
    for xl,yl,zl,Z in zip(x_list,y_list,z_list,init_Z):
        M_list.append((order,xl,yl,zl,Z))
        order+=1

    for order,x,y,z,charge in M_list:
        r = np.array((x,y,z))
        M_tmp = []
        for oorder,ox,oy,oz,ocharge in M_list:
            if oorder == order:
                IJ = 0.5*ocharge**2.4
                M_tmp.append(IJ)
            else:
                otr = np.array((ox,oy,oz))
                dist = np.linalg.norm(r-otr)
                InJ = (charge*ocharge)/dist
                M_tmp.append(InJ)
        M_matrix.append(M_tmp)

    M = np.array(M_matrix)
    w,v = np.linalg.eig(M)
    w_sort = np.sort(w)[::-1]

#    print np.amax(w_sort)

    return w_sort
