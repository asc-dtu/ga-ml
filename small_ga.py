"""A self contained GA class. That is designed to run once in a while with
an inexpensive fitness (raw_score) evaluation e.g. EMT or Machine Learning."""
from random import shuffle, choice
import string

from ase.ga.data import PrepareDB, DataConnection
from ase.ga.population import Population
from ase.ga import get_raw_score


class SmallGA(object):
    """A small self contained GA class.

    Parameters
    ----------
    input_candidates : list
        The starting candidates for the small GA run. It could be the current
        population or just randomly selected.
    relax_function : function
        The relax function should take an atoms object as input, calculate the
        raw_score and put it in atoms.info['key_value_pairs']['raw_score'].
    offspring_creator_list : list
        A list of offspring creators and weights. Just as described in
        the regular GA.
    population_pool : boolean
        Set whether the small GA should be a pool base or generation based
        run. Generally the generational run will be faster for a variational
        fitness function e.g. fitness sharing population.
    offspring_ok_function : function
        Function that should return True if a new candidate after creation
        is ok to put in the db.
        Default always ok.
        Example
        -------
        def is_ok(atoms):
            if len(set(atoms.numbers)) == 1:
                return False
            if ...
                return False
            return True
        SmallGA(..., offspring_ok_function=is_ok, ...)
    """

    def __init__(self, input_candidates, relax_function, offspring_creator_list,
                 db_file_name=None,
                 population_type=None,
                 population_kwargs=None,
                 population_size=20,
                 population_pool=True,
                 offspring_ok_function=None,
                 convergence_instance=None,
                 comparator=None,
                 verbose=False):
        
        self.relax_function = relax_function
        self.oclist = offspring_creator_list
        
        if db_file_name is None:
            db_file_name = ''.join([choice(string.letters + string.digits)
                                    for _ in range(8)]) + '.db'
        self.db_file_name = db_file_name
        
        if population_type is None:
            population_type = Population
        self.pop_type = population_type
        
        if population_kwargs is None:
            population_kwargs = {}
        self.pop_kwargs = population_kwargs
        
        if offspring_ok_function is None:
            offspring_ok_function = offspring_allways_ok
        self.offspring_ok_func = offspring_ok_function
        
        if convergence_instance is None:
            from ase.ga.convergence import NeverConvergence
            convergence_instance = NeverConvergence()
        self.convergence_instance = convergence_instance
        
        self.comparator = comparator
        self.pop_size = population_size
        self.population_pool = population_pool
        self.verbose = verbose

        self.initialize(input_candidates)
        
    def initialize(self, candidates):
        db = PrepareDB(db_file_name = self.db_file_name,
                       population_size = self.pop_size)
        
        sp = []
        for a in candidates:
            ac = a.copy()
            sp.append(ac)

        for a in sp:
            db.add_unrelaxed_candidate(a)

    def run(self, steps=100):
        """Run the small GA.

        Parameters
        ----------
        steps : integer
            The number of steps to run. For population_pool = True, steps is
            number of candidates tested. For population_pool = False, steps
            is number of generations.

        Returns
        -------
        list
            A list of the population after the number of steps taken.
        """
        db = DataConnection(self.db_file_name)

        while db.get_number_of_unrelaxed_candidates() > 0:
            a = db.get_an_unrelaxed_candidate()
            self.relax_function(a)
            db.add_relaxed_step(a)
        
        population = self.pop_type(data_connection=db,
                                   population_size=self.pop_size,
                                   comparator=self.comparator,
                                   **self.pop_kwargs)
        
        if self.verbose:
            s = 'step -1: max raw_score: {0}'
            print(s.format(get_raw_score(population.pop[0])))

        population.update()        

        step = 0
        if self.population_pool:
            while step < steps:
                if self.converged():
                    break
                step += 1
                a, desc = self.create_new_offspring(population)
                while not self.offspring_ok_func(a):
                    a, desc = self.create_new_offspring(population)
                db.add_unrelaxed_candidate(a, description=desc)

                self.relax_function(a)
                db.add_relaxed_step(a)

                population.update()
                if self.verbose:
                    s = 'step {1}: max raw_score: {0}'
                    print(s.format(get_raw_score(population.pop[0]), step))
        else:
            while step < steps:
                step += 1
                if self.converged():
                    break
                for i in range(self.pop_size):
                    a, desc = self.create_new_offspring(population)
                    while not self.offspring_ok_func(a):
                        a, desc = self.create_new_offspring(population)
                    db.add_unrelaxed_candidate(a, description=desc)

                    self.relax_function(a)
                    db.add_relaxed_step(a)

                population.update()
                if self.verbose:
                    s = 'step {1}: max raw_score: {0}'
                    print(s.format(get_raw_score(population.pop[0]), step))
        return population.pop
            
    def converged(self):
        return self.convergence_instance.converged()
        
    def create_new_offspring(self, population):
        operator = self.get_operator()
        
        parents = population.get_two_candidates()

        return operator.get_new_individual(parents)
        
    def get_operator(self):
        probabilities, oclist = self.oclist
        flat_oclist = []
        for p, oc in zip(probabilities,oclist):
            flat_oclist.extend(p*[oc])
        return choice(flat_oclist)
                
def offspring_allways_ok(*arg):
    return True
